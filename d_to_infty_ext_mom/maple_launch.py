import time
import os
from config import maple_path
import multiprocessing as mp


def maple_calc(diag, loops, order, half_spine_mode, linux):
    print(diag)
    if linux:
        os.system(maple_path + f' -q -t -c "loops:={loops}" -c "E_order:={order}" -c "half_spine_mode:={half_spine_mode}" -c "diag_nickel:={diag}" integrate.mpl')
    else:  # Windows
        os.system(maple_path + f' -q -t -c loops:={loops} -c E_order:={order} -c half_spine_mode:={half_spine_mode} -c diag_nickel:={diag} integrate.mpl')

if __name__ == "__main__":
    start_time = time.time()
    try:
        from config import *
    except Exception as e:
        print(e)

    if no_half_spine_account==True:
        dir_name = f'diags_{loops}_loops_no_halfspine'
    else:
        dir_name = f'diags_{loops}_loops'

    static_diags = os.listdir(os.path.join(os.getcwd(), dir_name, 'nonzero'))
    dyn_diags = []
    for sd in static_diags:
        with open(os.path.join(os.getcwd(), dir_name, 'nonzero', sd)) as fd:
            dyn_diags += [dd.strip() for dd in fd.readlines()]
    dyn_diags = list(map(lambda x: x.replace('|', 'i').replace(':', 'x').replace('A', 'b'), dyn_diags))

    if parallel:
        dyn_diags = list(map(lambda x: (x, loops, order, no_half_spine_account, linux), dyn_diags))
        with mp.Pool(mp.cpu_count()) as p:
            p.starmap(maple_calc, dyn_diags)
    else:
        for i, diag in enumerate(dyn_diags):
            print(i)
            maple_calc(diag, loops, order, no_half_spine_account, linux)
    print("--- %s seconds ---" % (time.time() - start_time))
