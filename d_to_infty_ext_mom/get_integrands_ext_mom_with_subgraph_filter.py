#! /usr/bin/python
#! encoding: utf8

__author__ = 'kirienko'

from d_to_infty_class_ext_mom import D_to_infty_graph as D
from sympy import var, symbols,  factor, simplify, gamma, Rational, collect, Symbol, sympify, prod
from itertools import product
import graph_state as gs
import graph_state_config_with_fields
from sign_account import sign_account
import os


def cut_edges(G,sub1,sub2):
    """
    :param G: initial networkx graph
    :param sub1: list of nodes of the subgraph 1 of G
    :param sub2: list of nodes of the subgraph 1 of G
    :return: list of the internal edges of G such that are cut
    """
    edges = list(G.subgraph(sub1).edges(keys=True)) + list(G.subgraph(sub2).edges(keys=True))
    return [e for e in G.edges(keys=True) if (e not in edges and -1 not in e and -2 not in e)]

def stretches_to_exponent(expr, stretch_vars, stretch_factors=None):
    """
    :param expr: sympy выражение
    :param stretch_vars: список sympy-переменных параметров растяжений, которые есть в expr
    :return: выражение, где все растяжения из множителей перешли в показатели
    """
    # Возвращает число слагаемых, при которых множителем стоит параметр растяжения stretch_factor
    def number_of_terms(expr, stretch_factor):
        s = expr.coeff(stretch_factor, 1)  # сумма при множителе stretch_factor
        number = str(s).count('+')
        if number > 0:
            number += 1
        elif number == 0 and str(s) != '0':
            number += 1
        return number
    stretch_degrees = dict(list(zip(stretch_vars, [number_of_terms(expr, st) for st in stretch_vars])))

    if len(stretch_vars) == 0:
        return expr

    expr_list = []
    aux_expr = expr.copy()
    while len(stretch_degrees) != 0:
        stretch = max(stretch_degrees, key=stretch_degrees.get)
        if stretch_degrees[stretch] != 0:
            expr_list.append((stretch, aux_expr.coeff(stretch, 1), aux_expr.coeff(stretch, 0)))
            aux_expr = aux_expr.coeff(stretch, 1)
        stretch_degrees.pop(stretch)

    final_expr = 0
    for i in expr_list[::-1]:
        if final_expr == 0:
            final_expr = i[1]
        final_expr = pow(final_expr, i[0]) + i[2]

    # Это условие срабатывает, когда во входном выражении нет растяжений, но сечение пересекает расх подграф.
    # Пример: e12|e3|34|5|55||:0A_aA_da|0a_dA|Aa_dA|da|aA_dd||
    # В таком случае просто возвращается исходное выражение дял сечения
    if final_expr==0:
        print('!!!!', expr)
        print(stretch_factors)
        return expr

    return final_expr

def subgraph_halfspine_check(subgraph):
    """
    Проверяет наличие полухребта у подграфа.
    """
    # Ищем внешние вершины
    G = graph_state_config_with_fields.from_str(str(subgraph))
    ext_nodes = tuple(gs.operations_lib.get_bound_vertices(G))
    # Проверяем, чтобы были рельсы, связывающие внешние вершины подграфа - это и есть полухребет
    if ext_nodes in D(str(subgraph)).bridges:
        return True
    else:
        return False

def integrand_with_ext_mom(graph_object, time_ver_number, no_half_spine_account):
    """
    Returns integrand ( = numerator/denominator) that corresponds certain time version tv.
    """
    version = graph_object.tv[time_ver_number]
    v = version[0]  # последовательность вершин из вр версии
    var(['k%d' % j for j in range(graph_object.Loops)])  # определяем импульсы интегрирования как sympy переменные
    var('p')
    var(['a%d' % j for j in range(graph_object.Loops - 1)])
    sq = lambda x: x ** 2
    all_momenta = [var('k%i' % i) for i in range(graph_object.Loops)]  # список переменных интегрирования
    all_momenta.append(var('p'))

    # Учет диаграмм без полухребта и вычитаний на подобные подграфы
    if no_half_spine_account == True:
        subgraph_list = graph_object.tv[time_ver_number][1]
    # Не учитываем диаграммы без полухребта и не делаем вычитания на сходящиеся подграфы(без полухбрета)
    else:
        subgraph_list = []
        for sub in graph_object.tv[time_ver_number][1]:
            if subgraph_halfspine_check(sub) == True:
                subgraph_list.append(sub)


    # ---------------------------------НАПИСАНИЕ МНОЖИТЕЛЕЙ В ЗНАМЕНАТЕЛЕ-----------------------------------------------
    denominator = 1
    den_list = []  # список множителей в знаменателе

    # Словарь всех линий диаграммы и импульсов на них вида {edge1: mom1, ...}
    #mom_on_edges = {e: graph_object.momenta_in_edge_with_ext_momentum(e) for e in graph_object.U.edges} старое !!!!!!!!!!!!!!!!!!!!!!!
    mom_on_edges = {e: graph_object.momenta_in_edge_with_ext_momentum(e) for e in graph_object.U.edges(keys=True)}
    # Убираем из словаря внешние линии без импульсов на них
    mom_on_edges = {e: mom for e, mom in mom_on_edges.items() if mom}

    # Составляем словарь, где подграфу сопоставляются его растяжение, втекающий и циркулярующие импульсы вида
    # {sub0: {stretch: a0, ext_mom: sum of moms, simple_mom: [simple_mom1, ...]}, ...}
    gs_graph_object = graph_object.dynamic_diag(graph_object.nickel)  # продублировали диаграмму в формате графстейта
    sub_info = dict()
    for i, sub in enumerate(subgraph_list):
        # Определяем растяжение:
        sub_info[sub] = dict()
        sub_info[sub]['stretch'] = var(f'a{i}')
        # Определяем втекающий импульс:
        sub_bound_vertices = sub.get_bound_vertices()
        sub_external_edges = []
        for vert in sub_bound_vertices:
            for edge in gs.operations_lib.edges_for_node(gs_graph_object, vert):
                if edge not in sub.edges():
                    sub_external_edges.append(tuple(list(edge.nodes) + [0]))
        ext_mom1 = mom_on_edges[sub_external_edges[0]]
        ext_mom2 = mom_on_edges[sub_external_edges[1]]
        if ext_mom1 != ext_mom2:
            error_message = f'В диаграмме {graph_object.nickel} в подграф {str(sub)} втекают разные импульсы с двух сторон!'
            raise (error_message)
        sub_info[sub]['ext_mom'] = ext_mom1
        # Определяем простые импульсы:
        # !!!!!!!!!!!!!!!!!!
        sub_nodes = [n for n in sub.vertices if n > -1]  # список вершин по Никелю, которые входят в расх подграф вр версии
        sub_networx = graph_object.U.subgraph(sub_nodes)  # подграф по вершинам в networkx

        # Исправить строку ниже - не работает  для 5 петель!
        # Линия подграфа записывается иначе, чем та же линия во всей диаграмме (перепутаны номера вершин)
        #sub_info[sub]['simple_mom'] = {var(mom_on_edges[e][0]) for e in sub_networx.edges(keys=True)
        #                   if len(mom_on_edges[e]) == 1}

        # Ниже костыльный фикс, который работает по крайней мере для одной 5петлевой (проверено)
        # Бдует работать для всех, если вершины в нетворксовых линиях основной диаграммы ВСЕГДА записываются по возрастанию!
        sub_info[sub]['simple_mom'] = {var(mom_on_edges[tuple(sorted(e[0:2])+[e[2]])][0]) for e in sub_networx.edges(keys=True)
                           if len(mom_on_edges[tuple(sorted(e[0:2])+[e[2]])]) == 1}

    # Все параметры растяжения, которые присутствуют во вр версии - понадобится для вычитаний:
    stretch_vars_all = [(sub_info[sub]['stretch'], sub) for sub in sub_info]

    # Цикл по временным сечениям
    for l in range(1, len(v)):
        # Определим список подграфов, пересеченных сечением crossed_subs.
        # Для этого вставим в последовательность вершин вр версии сечение в нужное место и
        # посмотрим на положения концов каждого подграфа относительно положения сечения
        crossed_subs = []
        tv_with_cut = v.copy()
        tv_with_cut.insert(l, 'cut')
        for sub in subgraph_list:
            sub_nodes = [n for n in sub.vertices if n > -1]  # номера вершин подграфа
            sub_nodes_pos_in_tv = [tv_with_cut.index(node) for node in sub_nodes]
            if min(sub_nodes_pos_in_tv) < l < max(sub_nodes_pos_in_tv):
                crossed_subs.append(sub)

        # Отсортируем так, чтобы бы сначал шли меньшие подграфы (у них длина индекса Никеля, соответственно, меньше)
        # Это важно, тк на этом держится расстановка степенных растяжений
        crossed_subs.sort(key=len)

        # Линии временного сечения; сделано через networx
        edges = cut_edges(graph_object.U, v[:l], v[l:])
        cut_factor = 0  # список слагаемых в множителе, соответствущем сечению
        # Составим словарь из линий сечения и их энергий. Его и будем модифицировать добавлением растяжений.
        # Степенные растяжения будут добавляться прямо в энергии, а
        # линейные растяжения записываются отдельно в список, соответствующий каждой линии. Изначально он пустой.
        # Таким образом, вид словаря следующий: {edge1: [energy_on_edge1, []], edge2: [energy_on_edge2, []], ...}
        energy_on_cut_edges = {e: [sum(list(map(var, mom_on_edges[e]))), []] for e in edges}
        for e in edges:
            for sub in crossed_subs:
                if any(var(mom) in sub_info[sub]['simple_mom'] for mom in mom_on_edges[e]):
                    if not all(var(mom) in sub_info[sub]['simple_mom'] for mom in mom_on_edges[e]):
                        #simple_mom_on_edge = [var(mom) for mom in mom_on_edges[e] if var(mom) in sub_info[sub]['simple_mom']]
                        exp_stretched_energy = energy_on_cut_edges[e][0].subs([(mom, 0) for mom in sub_info[sub]['simple_mom']])
                        exp_stretched_energy = exp_stretched_energy.subs([(stretch[0], 1) for stretch in stretch_vars_all])
                        if exp_stretched_energy!=0:
                            energy_on_cut_edges[e][0] = energy_on_cut_edges[e][0].subs(exp_stretched_energy, exp_stretched_energy**sub_info[sub]['stretch'])
                else:
                    energy_on_cut_edges[e][1].append(sub_info[sub]['stretch'])

        for edge in energy_on_cut_edges:
            cut_factor += energy_on_cut_edges[edge][0] * prod(energy_on_cut_edges[edge][1])

        cut_factor = cut_factor.subs([(mom_var, sq(mom_var)) for mom_var in all_momenta])

        denominator *= cut_factor
        den_list.append(cut_factor)


    # НАПИСАНИЕ МНОЖИТЕЛЕЙ В ЧИСЛИТЕЛЕ-----------------------------------------------------------------------------------------
    gs_graph_object = graph_object.dynamic_diag(graph_object.nickel)  # продублировали диаграмму в формате графстейта
    numerator = 1
    num_list = []  # список множителей в числителей

    # Проверяем все подграфы во вр версии(???) на наличие полухребта.
    # Если его нет, то на одни рельсы с втекающим импульсом в подграфе растяжение делать НЕ надо -
    # он сократится с 1/(втек_имп)^2 из правила вычитания. Пока такие рельсы не учтены -
    # статус подграфа False, после учета он меняется на True, и начинают работать стандартные правила.
    subgraph_info = dict()  # список подграфов и втекающих в них импульсов
    # Структура:
    # subgraph_info = {subgraph.nickel: [множество_втек_импульсов, список_рельсов_внутри_подграфа, False], ...}
    for subgraph in subgraph_list:
        sub_bound_vertices = subgraph.get_bound_vertices()

        sub_external_edges = []
        for v in sub_bound_vertices:
            for edge in gs.operations_lib.edges_for_node(gs_graph_object, v):
                if edge not in subgraph.edges():
                    sub_external_edges.append(tuple(list(edge.nodes) + [0]))
        ext_mom1 = graph_object.momenta_in_edge_with_ext_momentum(sub_external_edges[0])
        ext_mom2 = graph_object.momenta_in_edge_with_ext_momentum(sub_external_edges[1])
        if ext_mom1 != ext_mom2:
            error_message = f'В диаграмме {graph_object.nickel} в подграф {str(subgraph)} втекают разные импульсы с двух сторон!'
            raise (error_message)

        subgraph_bridges = []  # список рельсов в подграфе
        all_nodes = set([x for x in subgraph.vertices if x > -1])
        for b in graph_object.bridges:
            if b[0] in all_nodes and b[1] in all_nodes:
                subgraph_bridges.append(b)

        subgraph_info[subgraph] = [set(map(var, ext_mom1)), subgraph_bridges, False]

    # Список вершин, формирующих хребет
    spine_list = set()
    for edge in graph_object.spine:
        spine_list.update(edge.nodes)

    for b_num, bridge in enumerate(graph_object.bridges):
        test = [int(graph_object.flow_near_node(x), 2) for x in bridge]
        shared_mom_bin = bin(test[0] & test[1])[2:]
        shared_mom = [var("k%d" % j) for j, k in enumerate(shared_mom_bin[::-1]) if int(k) and j < graph_object.Loops]

        # Проверяем, входят ли оба конца рельсов в хребет.
        # Если нет, то внешнего импульса в рельсах не будет, и работает старый код.
        if bridge[0] in spine_list and bridge[1] in spine_list:
            shared_mom.append(var('p'))

        if shared_mom == [var('p')]:
            num_list.append(var('p') ** 2)
            continue

        num = 0
        stretch_vars_bridges = set()  # cписок параметров растяжений в рельсах
        for m in shared_mom:
            term = m ** 2
            for s_num, sub in enumerate(subgraph_list):
                internal_mom = graph_object.subgraph_simple_momenta(sub)
                if bridge in subgraph_info[sub][1]:  # Если рельсы лежат внутри подграфа
                    if set(shared_mom) == subgraph_info[sub][0]:  # Если все импульсы в рельсах являются втек в подграф
                        if subgraph_info[sub][2] == False:
                            subgraph_info[sub][2] = True
                            b_ind = b_num
                            s_ind = s_num
                            continue
                        elif b_num == b_ind and s_num == s_ind:
                            continue
                    if m in subgraph_info[sub][0] and set(subgraph_info[sub][0]).issubset(shared_mom):  # Если импульс втек в подграф, и среди моментов рельс присутствуют все втек в подграф ипульсы
                        term *= var('a%d' % s_num)
                        stretch_vars_bridges.add(var('a%d' % s_num))
            num += term

        if len(stretch_vars_bridges) != 0:
            final_expr = stretches_to_exponent(num, stretch_vars_bridges)
            numerator *= final_expr
            num_list.append(final_expr)
        else:
            numerator *= num
            num_list.append(num)

    # Возвращает список множителей в числителе(num_list) и полное выражение в знаменателе (denominator)
    # Так надо для Фейнмана
    #print(graph_object.nickel)
    #print(num_list)
    #print('-------------')
    return num_list, den_list, list(stretch_vars_all)


def sympy_to_maple(sp_expr):
    """
    sympy выражение переводит в мэпловское
    :param sp_expr: sympy выражение
    :return: maple_expr: мэпловская команда
    """
    maple_expr = str(sp_expr)
    maple_expr = maple_expr.replace("gamma", "GAMMA")
    return maple_expr


def substration(num_list, den_list, stretch_vars):
    """
    :param num_list: список множителей в числителе с растяжениями
    :param den_list: список множителей в знаменателе с растяжениями
    :param stretch_vars: множество пар (переменная_растяжения, номенклатура соотв. ей подграфа)
    :return: список интегрлов после подстановки [[[список множителей в числителе],[список множителей в знаменателе], знак перед интегралом(1 или -1)], ...]
    """
    perms = list(product([0, 1], repeat=len(stretch_vars)))
    sympy_subs = []

    for perm in perms:
        sub = list(zip([v[0] for v in stretch_vars], perm))
        sub = list(map(list, sub))
        if perm.count(0)%2 == 0:
            sign = 1
        else:
            sign = -1
        sympy_subs.append([sub, sign])
    subs_list = []
    for sub in sympy_subs:
        subs_list.append([list(map(lambda x: x.subs(sub[0]), num_list)),
                          list(map(lambda x: x.subs(sub[0]), den_list)),
                          sub[1]])
    return subs_list

def feyn_repr(integrand, graph_object):
    """
    Фейнмановское представление с учетом множителей в числителе
    ВАЖНО: эта функция работает только если все множители и в числителе, и в знаменателе имеют первую степень!
    :param integrand: [[список множителей в числителе],[список множителей в знаменателе], знак перед интегралом(1 или -1)] - подинтегральное выражение (подстановка растяжек 0 1 уже сделана)
    :return: param_int - подинтергральное выражение, которое пойдет в hyper_int
    :return: diff_vars - список переменных, соотв множителям из числителя, по которым будем дифф в мэпле
    :return: non_int_factor - внеинтегральный множитель
    """
    # Убираем множители в числителе, равные 1 (просто мешаются)
    integrand[0] = [i for i in integrand[0] if i != 1]  # убираем множители в числителе, равные 1 (просто мешаются)

    # Считаем, сколько множителей в числители просто p**2 и выносим их в отдельный внеинт множитель
    p_factor = 1/var('p')**2  # из условия нормировки считаем такой объект
    p_factor *= (var('p')**2)**(integrand[0].count(var('p')**2))
    integrand[0] = [i for i in integrand[0] if i != var('p')**2]

    m = len(integrand[0])  # кол-во множителей в числителе
    n = len(integrand[1])  # кол-во множителей в знаменателе
    subs_sign = integrand[2]  # знак от R'-вычитаний

    # Написание внеинтегрального множителя
    d, E = symbols('d E')
    l = graph_object.Loops
    alpha = n - m
    sign_from_mom_flow = sign_account(graph_object)
    non_int_factor = (subs_sign * sign_from_mom_flow * p_factor * Rational(2**(-2*l)) * gamma(alpha - d*l/2) * (gamma(d/2))**(l)).subs(d, 2-2*E)

    all_factors = integrand[1] + integrand[0]
    u_list = [var('u%d'%(i + 1)) for i in range(n + m)]
    quad_form = 0
    var_list = set()

    for i in range(m + n):
        var_list = var_list.union(all_factors[i].free_symbols)
        quad_form += u_list[i] * all_factors[i]
    var_list.remove(var('p'))
    quad_form = quad_form.factor()

    det = 1
    c = quad_form.copy()
    for v in var_list:
        mom_coeff = quad_form.coeff(v**2)
        det *= mom_coeff
        c -= mom_coeff*v**2
    c = simplify(c)

    l_old = len(var_list)  # число петель
    if l_old != l:
        with open('/d_to_infty_ext_mom/4l_kirienko.txt', 'a') as f:
            f.write(f'l!=l_old in {graph_object} \n')
    if p_factor==1/var('p')**2:
        with open('/d_to_infty_ext_mom/4l_kirienko.txt', 'a') as f:
            f.write(f'p_factor = 1/p**2 in {graph_object} \n')

    param_int = det**(-d/2) * c**(-(n-m)+d*l/2)

    diff_vars = str([var('u%d'%(n + i + 1)) for i in range(m)])  # список переменных, соотв множителям из числителя, по которым будем дифф в мэпле
    param_int = param_int.subs([(d, 2 - 2 * E)])  # подинтергральное выражение, которое пойдет в hyper_int
    return sympy_to_maple(non_int_factor), sympy_to_maple(param_int), diff_vars


def integrand_maple(graph_obj):
    """
    :param graph_obj: instance of D_to_infty_graph class
    :return: integrand as a string
    """
    tv = graph_obj.get_time_versions()
    # В integrands соберем все пары (param_int, non_int_expr) со всех вычитаний во всех вр версиях.
    # Этот список полностью определяет диаграмму.
    integrands = []
    for tv_num, ver in enumerate(tv):       ## Loop over time versions
        #all_int_of_tv = substration(*integrand_with_ext_mom(graph_obj, tv_num))  ## list of all integrands of certain tv
        all_int_of_tv = substration(*integrand_with_ext_mom(graph_obj, tv_num))  # НОВЫЕ РАСТЯЖЕНИЯ
        integrands += list(map(lambda sub: feyn_repr(sub), all_int_of_tv))
    return integrands

