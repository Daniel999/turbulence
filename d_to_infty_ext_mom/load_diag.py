### req gs >=1.1.1
import re

import graph_state

import graph_state_config_with_fields_nums as gs_config
import sys
import os
import graphine
import itertools
import turbulence as turb


dir0 = sys.argv[1]
dir1 = dir0 + '/nonzero/'
dir2 = dir0 + '/ans/'

for name in os.listdir(dir2):
    print(name)

    for line in os.listdir(dir2+name):
        print()


        gn = line.replace('-', '|').replace('x', ':').replace('b','A').strip()

        gs = gs_config.gs_builder.graph_state_from_str(gn +'::')

        ext, vlist = list(gs.nodes)[0], list(gs.nodes)[1:]
        ext2 = ext.copy(nv=-1)

        cts = 0
        lcts = []
        for vl in itertools.permutations(range(len(vlist))):
            vl = enumerate(vl)
#            print([x for x in vl])
            vlist2 = list([vlist[x1].copy(nv=x2) for x1,x2 in vl])+[ext2]
            elist = []

            for i, edge in enumerate(gs.edges):

                nodes = vlist2[edge.nodes[0].index],vlist2[edge.nodes[1].index]
                edge2 = gs_config.new_edge(nodes,fields=edge.fields,n=i)
                elist.append(edge2)
            g2 = gs_config.graph(elist)
#            print(g2)

            stop = False
            for e in g2.internal_edges:
                if (e.fields.pair[1] == 'A' and  e.nodes[0].nv > e.nodes[1].nv) or (e.fields.pair[0] == 'A' and  e.nodes[0].nv < e.nodes[1].nv):
                    stop = True
                    break
            if not stop:
#                print(g2)
                cnt = 1
                for x in turb.x_forest_generator(g2):
                    cnt += 1
#                    print([xx for xx in x])
                cts += cnt
                lcts.append(cnt)
#                print([x for x in g2.x_relevant_sub_graphs(filters=turb.one_irreducible + turb.uv_relevant)])
                print(g2, cnt)
        print(lcts, cts)
        print(line)
        fname = line[:line.find('x')].replace('|', '-')
#        print(fname)
        gname = line.replace('|', '-').replace(':', 'x').replace('A', 'b').strip()
        dir3 = f'{dir2}/{fname}/{gname}'
        print(dir3)
        ls = os.listdir(dir3)
#        print(ls)
        regex = 'sd_polys_tv(\d+)_term(\d+).txt'
        tvs = {}
        for file in ls:
            m = re.match(regex,file)
            if m is not None:
#                print(m.groups())
                tv = int(m.groups()[0])
                ct = int(m.groups()[1])
                if tv not in tvs:
                    tvs[tv] = list()
                tvs[tv].append(ct)
        ntvs = 0
        lntvs = list()
        for tv in tvs:
            lntvs.append( len(tvs[tv]))
            ntvs += len(tvs[tv])

        if lcts == lntvs:
            print("Ok")
        else:
            print(lcts, cts)
            print(lntvs, ntvs)
            print('CHECK')
            if cts == ntvs:
                print('Ok')
            else:
                print('Fail')