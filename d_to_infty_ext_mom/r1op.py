### req gs >=1.1.1
import copy

import graph_state

import graph_state_config_with_fields_nums as gs_config
import sys
import os
import graphine
import turbulence as turb






graph = gs_config.from_str('e12|e3|44|55|5||:0A_aA_dA|0a_dA|aA_dd|aa_dd|aA||:0|1|2|3|4|5:0_1_2|3_4|5_6|7_8|9||')
print(graph)


cnt = 1
print(cnt, graph)
debug = False
print('-------------------')
for x in graph.edges():
    print((x.nodes[0].index,x.nodes[1].index),x.n)
print()
print('----------------')



ct_components = set()
debug = True
for f in turb.x_forest_generator(graph):
    ct = turb.shrink_forest(graph, f)
    if debug:
        print(111,(f))
        print(cnt+1, ct)
    edges = list(ct[0].edges())

    for g in turb.renumber(ct[1:]):
        edges += g.edges() if g in f else g

#    e, w = edges2ep(edges, weights_to_expr=True, bphz=True)
#    res = e + [(-1)**len(f)] + [w]
#    ct_components = ct_components | set(map(lambda x: remove_idx(x), ct))

    cnt += 1
    print((-1)**len([x for x in f]))
    print('-------------------')
    for x in edges:
        print((x.nodes[0].index,x.nodes[1].index),x.n)
    print()
#    hi_text.append('G[%s]:=%s: \n' % (cnt, res))


    print('----------------')
cnt = 1
for x in turb.x_forest_generator(graph):
    cnt += 1
    print([xx for xx in x])

print(graph, cnt)