from graphine.filters import one_irreducible, graph_filter, connected
import itertools
import graphine
import re

@graph_filter
def uv_relevant(edges_list, super_graph):
    sub_graph = graphine.Representator.asGraph(edges_list)
#    print(sub_graph)
    if sub_graph.external_edges_count != 2:
        return False
    extf = sub_graph.external_edges[0].fields.pair[1], sub_graph.external_edges[1].fields.pair[1]
    if not (extf == ('A', 'a') or extf == ('a', 'A')):
        return False
#    print('-----1')
#    print(sub_graph.vertices)
#    print([x for x in map(lambda x: x.nv, sub_graph.vertices)])

    maxv = max(map(lambda x: x.nv, sub_graph.vertices-{-1}))
    minv = min(map(lambda x: x.nv, sub_graph.vertices-{-1}))
#    print(maxv,minv)
    if (maxv-minv) != 2*sub_graph.loops_count-1:
        return False
#    print('-----2')
    if 2*sub_graph.loops_count == len(sub_graph.vertices)-1:
        return True
    else:
        return False

subgraphs =   eval(open('subgraphs.py').readline())
#subgraphs = subgraphs | set('e11|e|:0A_aA_dd|0a|')
@graph_filter
def uv_relevant2(edges_list, super_graph):
    sub_graph = graphine.Representator.asGraph(edges_list)
#    print(str(sub_graph))
    m = re.match('([^:]+:[^:]+).*', str(sub_graph))
    if m is None:
        return False
    ss = m.groups()[0]
#    print(sub_graph, ss, len(subgraphs),ss not in subgraphs)
    if ss not in subgraphs:
        return False

    maxv = max(map(lambda x: x.nv, sub_graph.vertices-{-1}))
    minv = min(map(lambda x: x.nv, sub_graph.vertices-{-1}))
#    print(maxv,minv)
    if (maxv-minv) != 2*sub_graph.loops_count-1:
        return False
#    print('-----2')
    if 2*sub_graph.loops_count == len(sub_graph.vertices)-1:
        return True
    else:
        return False


def contain(g1, g2):
    if g2 is None:
        return False
    return set(g2.internal_edges) & set(g1.internal_edges) == set(g2.internal_edges)


def x_forest_generator(graph):
    subgraphs = [x for x in graph.x_relevant_sub_graphs(filters=one_irreducible + uv_relevant2)]
#    print(subgraphs)
#    if set(map(uv_index, subgraphs)) != {0} and not len(subgraphs)==0:
#        raise NotImplementedError("non logarithmic subgraphs %s" % subgraphs)

    for i in range(1, len(subgraphs)+1):
        for comb in itertools.combinations(range(len(subgraphs)), i):
            valid_forest = True
            for s1, s2 in itertools.combinations(comb, 2):
                sg1 = subgraphs[s1]
                sg2 = subgraphs[s2]
                if contain(sg1, sg2) or contain(sg2, sg1):
                    continue
                if graphine.util.has_intersecting_by_vertices_graphs([sg1, sg2]):
                    valid_forest = False
                    break
            if valid_forest:
                yield list(map(lambda x: subgraphs[x], comb))


def shrink_forest(graph, forest):
    res = [graph]
    sorted_forest = sorted(forest, key=lambda x: len(x.internal_edges), reverse=True)
    for f in sorted_forest:
        res_1 = list()
        for g in res:
            if contain(g, f):
                shrunk = g.shrink_to_point(f.internal_edges)
                res_1.append(shrunk)
                res_1.append(f)
            else:
                res_1.append(g)
        res = res_1
    return res


def renumber(graphs):
    idx = x_index(100)
    res = list()
    for g in graphs:
        vertices = g.vertices-{-1, 1000}
        v_map = {-1: -1, 1000: 1000}
        for v in vertices:
            v_map[v] = next(idx)
        edges = list()
        for edge in g.edges():
            edges.append(edge.copy(node_map=dict(map(lambda x: (x, v_map[x]), edge.nodes))))
        res.append(graphine.Graph(edges, renumbering=False))
    return res


def x_index(shift):
    idx = shift
    while True:
        yield idx
        idx += 1
