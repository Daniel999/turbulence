read "HyperInt.mpl":
hyper_verbosity := 10:
_hyper_check_divergences := false:

with(StringTools):
dynamic_nickel := SubstituteAll(diag_nickel,"i","-"):
static_nickel := Split(dynamic_nickel, "x")[1]:
if convert(half_spine_mode,string) = "True" then
    path := FileTools[JoinPath]([currentdir(), cat("diags_",convert(loops,string),"_loops", "_no_halfspine"), "ans", static_nickel, dynamic_nickel]):
else
    path := FileTools[JoinPath]([currentdir(), cat("diags_",convert(loops,string),"_loops"), "ans", static_nickel, dynamic_nickel]):
end if:
terms_txts := FileTools[ListDirectory](path): #список всех файлов
terms_txts_filtered := []:
for file_name in terms_txts do
    if substring(file_name,1..2 )="tv" then
        terms_txts_filtered := [op(terms_txts_filtered), file_name]
    end if:
end do:

with(MultiSeries, series):
res_txts := []:
for i in terms_txts_filtered do
    writeto(FileTools[JoinPath]([path, cat("order", convert(E_order,string), "_", SubstituteAll(i,".txt",""), "_log.txt")])):
    non_int_factor := parse(readline(FileTools[JoinPath]([path,i]))):
    f := parse(readline(FileTools[JoinPath]([path,i])));
    f := f*non_int_factor;
    printf(cat("f = non_int_factor*integrand = ", convert(f, string), "\n"));
    diff_vars := readline(FileTools[JoinPath]([path,i])):
    fclose(FileTools[JoinPath]([path,i])):
    diff_vars := Split(SubstituteAll(substring(diff_vars, 2 .. -2), " ", ""), ","):
    diff_vars := map(parse, diff_vars);
    printf(cat("diff_vars = ", convert(diff_vars, string), "\n"));
    for var in diff_vars do
        f:=-diff(f,var):
        f:=subs(var=0,f);
    end do:
    printf(cat("f_after_diff_u = ", convert(f, string), "\n"));
    f:=-subs(p=1,diff(f,p));
    printf(cat("f_after_diff_p = ", convert(f, string), "\n"));

    #L:=[seq(u||i,i=1..parse(convert(diff_vars[1],string)[2..])-1)]:
    L:=convert(indets(f, name) minus {E, p}, list):
    f_coeff := coeff(convert(series(f, E = 0, E_order+1),polynom,E), E, E_order):
    printf(cat("coeff_E_integrand = ", convert(f_coeff, string), "\n"));
    hyper_int_res := hyperInt(eval(f_coeff, L[1] = 1), L[2..]):
    res := collect(fibrationBasis(hyper_int_res, []), E);
    printf(cat("res = ", convert(res, string), "\n"));

    res_name := cat("order", convert(E_order,string), "_", SubstituteAll(i,".txt",""), "_ans.txt"):
    res_txts := [op(res_txts), res_name]:
    fd:=FileTools[Text][Open]( FileTools[JoinPath]([path, res_name])):
    FileTools[Text][WriteLine](fd, convert(res,string)):
    FileTools[Text][Close](fd):
end do:

writeto(terminal):
# Collect terms together
final_res := 0:
for i in res_txts do
    term := parse(readline(FileTools[JoinPath]([path,i]))):
    fclose(FileTools[JoinPath]([path,i])):
    final_res := final_res + term:
end do:

fd:=FileTools[Text][Open]( FileTools[JoinPath]([path, cat("order", convert(E_order,string), "_ans.txt")])):
FileTools[Text][WriteLine](fd, convert(final_res,string)):
FileTools[Text][Close](fd):
