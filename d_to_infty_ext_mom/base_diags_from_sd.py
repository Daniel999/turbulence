import os
import copy
from sympy import *
try:
    from config import *
except Exception as ex:
    print(ex)

with open(os.path.join(os.getcwd(), 'sd_results', f'{loops}loops.txt')) as f:
    data = f.readlines()
data = list(map(lambda x: x.replace('\n','').strip(), data))

sd_dict = dict()
term_num = 0
nickel = None
new_diag_switch = False
term_ans = sympify(0)  # промежуточный ответ
import re
for line in data:
    if 'xxxxxxxxx' in line:
        nickel_old = copy.deepcopy(nickel)
        nickel = line.split('/')[-1].split(' ')[0]
        nickel = nickel.replace('-','|').replace('x',':').replace('b','A')
        sd_dict[nickel] = sympify(0)

    elif len(line) == 0:
        new_diag_switch = True

    elif line[:2] == 'sd':
        term_num_new = int(line.split('term')[1].split('.txt')[0])
        if term_num_new <= term_num:
            if new_diag_switch == False:
                sd_dict[nickel] += term_ans
            else:
                sd_dict[nickel_old] += term_ans
                new_diag_switch = False
        term_num = term_num_new
        term_ans = line.split('txt')[1].strip()
        term_ans = sympify(re.sub("\(.*?\)", "", term_ans))
sd_dict[nickel] += term_ans


for diag in sd_dict:
    sd_dict[diag] /= 2*loops*var('e')
    sd_dict[diag] = simplify(sd_dict[diag])
#    print(diag, '-->', sd_dict[diag])
#print('--------------------------------------------')

# Разбиваем по топологиям - суммируем вклады всех диаграмм с одинаковой статической топологией
sd_topology_dict = dict()
for diag in sd_dict:
    static_nickel = diag.split(':')[0]
    if static_nickel in sd_topology_dict:
        sd_topology_dict[static_nickel] += sd_dict[diag]
    else:
        sd_topology_dict[static_nickel] = copy.deepcopy(sd_dict[diag])

for diag in sd_topology_dict:
    print(diag, '-->', sd_topology_dict[diag])


# Проверка общей суммы базовых диаграмм
#sum = 0
#for diag in sd_topology_dict:
#    sum+=sd_topology_dict[diag]
#print(sum)

#print('-0.015625/(e^2) - 0.013848366/e - 0.10472755 - 0.06269719*e')   # суммарный ответ 2loop от ЛЦ
#print('0.003255209/(e^3) + 0.00286285/(e^2) + 0.0479697/e + 0.0145874')  # суммарный ответ 3loop от ЛЦ
