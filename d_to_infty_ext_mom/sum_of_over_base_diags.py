from get_integrands_ext_mom_with_subgraph_filter import *
from sympy import sympify, symbols
import ast
try:
    from config import *
except Exception as e:
    print(e)

zeta2 = symbols('zeta2')
zeta3 = symbols('zeta3')
zeta4 = symbols('zeta4')
zeta5 = symbols('zeta5')

new_tv_ans = dict()
with open(os.path.join(os.getcwd(), f'diags_{loops}_loops', f'diags_{loops}_loops_{order}order_tv_answers.txt')) as f:
    res_text = f.readlines()
for i in range(len(res_text)):
    if i%2==0:
        new_tv_ans[res_text[i].replace('\n', '').strip()] = ast.literal_eval(res_text[i+1].replace('\n', '').strip())
for d in new_tv_ans:
    for tv in new_tv_ans[d]:
        new_tv_ans[d][tv] = sympify(new_tv_ans[d][tv])
print(new_tv_ans)


tv_count = 0
tv_over_base_main_count = 0
tv_over_base_main_and_sub_count = 0
sum_over_base = 0
# Сравниваем ответы для всех временных версий старым и новым способом растяжений
for d in new_tv_ans:
    for tv in new_tv_ans[d]:
        tv_count+=1
        diag = D(d.replace('-', '|').replace('x',':').replace('b','A'))
        tv_seq = diag.get_time_versions()[int(tv.split('v')[1])]
        tv_status_over_main = diag.tv_over_main_base_check(tv_seq)  # True, если ничего вылазит за базу
        if tv_status_over_main==False:
            tv_over_base_main_count += 1
        tv_status_over_sub_and_main = diag.tv_over_main_and_subgraph_base_check(tv_seq)  # True, если ничего вылазит за базу
        if tv_status_over_sub_and_main==False:
            tv_over_base_main_and_sub_count += 1
            sum_over_base += sympify(new_tv_ans[d][tv])

print('Количество вр версий: ', tv_count)
print('Количество вр версий, выходящих за основную базу: ', tv_over_base_main_count)
print('Количество вр версий, выходящих за хоть одну базу: ', tv_over_base_main_and_sub_count)
print('Сумма вр версий, выходящих за подграф: ', sum_over_base)

