import time
import os
from os.path import join
from os.path import exists
import multiprocessing as mp


def diags_from_static(loops, half_spine_mode):
    """
    :param loops: number of loops
    :param half_spine_mode: switch for accounting diags with no halfspine
    :return: list of diagrams by listing `nonzero` directory
    """
    if half_spine_mode==True:
        dir_name = f'diags_{loops}_loops_no_halfspine'
    else:
        dir_name = f'diags_{loops}_loops'
    static_diags = os.listdir(os.path.join(os.getcwd(), dir_name, 'nonzero'))
    dyn_diags = []
    for sd in static_diags:
        with open(os.path.join(os.getcwd(), dir_name, 'nonzero', sd)) as fd:
            dyn_diags += [dd.strip() for dd in fd.readlines()]
    return dyn_diags

def prep_integrand(diag):
    from sign_account import sign_account
    from d_to_infty_class_ext_mom import D_to_infty_graph as D
    from get_integrands_ext_mom_with_subgraph_filter import integrand_with_ext_mom, substration, feyn_repr
    from config import no_half_spine_account

    print(diag)
    static_nickel = diag.split(':')[0].replace('|','-')
    static_nickel_dir = join(os.getcwd(), static_nickel)
    if not exists(static_nickel_dir):
        os.mkdir(static_nickel_dir)
    os.chdir(static_nickel_dir)

    dynamic_nickel = diag.replace('|', '-').replace(':','x').replace('A','b')
    if not exists(dynamic_nickel):
        os.mkdir(dynamic_nickel)
    os.chdir(dynamic_nickel)

    # Gathering together all info about diag:
    diag_obj = D(diag)
    loops = diag_obj.Loops
    output = []
    output.append(diag)
    tv = diag_obj.get_time_versions()
    output.append('Time versions: '+ str(tv))
    output.append('Number of time versions: ' + str(len(tv)))
    output.append('Sign from momenta flow: ' + str(sign_account(diag_obj)))
    output.append('***************************************************************************************************')
    for tv_num, ver in enumerate(tv):
        all_int_of_tv = substration(*integrand_with_ext_mom(diag_obj, tv_num, no_half_spine_account))  # list of all integrands of certain tv
        output.append('tv'+str(tv_num) + ' ' + str(ver))
        for sub_num, sub in enumerate(all_int_of_tv):
            output.append(f'term{sub_num}:')
            output.append(str(sub))
            feyn = feyn_repr(sub, diag_obj)
            output.append(str(feyn))
            with open(f'tv{tv_num}_term{sub_num}.txt', 'w') as f:
                f.write(str(feyn[0])+'\n')
                f.write(str(feyn[1])+'\n')
                f.write(str(feyn[2]))
        output.append('------------------------------')

    with open('info.txt', 'w') as f:
        f.write('\n'.join(output))
    os.chdir('../..')


if __name__ == "__main__":
    start_time = time.time()
    try:
        from config import *
    except Exception as e:
        print(e)

    if no_half_spine_account==True:
        dir_name = f'diags_{loops}_loops_no_halfspine'
    else:
        dir_name = f'diags_{loops}_loops'

    ans_dir = join(os.getcwd(), dir_name, 'ans')
    if not exists(ans_dir):
        os.mkdir(ans_dir)
    diags = diags_from_static(loops, half_spine_mode=no_half_spine_account)
    os.chdir(ans_dir)

    if parallel:
        with mp.Pool(mp.cpu_count()) as p:
            p.map(prep_integrand, diags)
    else:
        for i, d in enumerate(diags):
            print(i)
            prep_integrand(d)
    print("--- %s seconds ---" % (time.time() - start_time))