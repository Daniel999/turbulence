# Эта программа сравнивает подиаграммно ответы, посчитанные через гиперлогарифмы и sd.
# Подынтегральные выражения при этом одинаковые и генерятся нашим алгоритмом.
# Ответы sd бедрутся из папки sd. Ответы Панцера берутся из папок с ответами.

import os
import ast
import copy
from sympy import *
try:
    from config import *
except Exception as ex:
    print(ex)

# Если коэффициент при любом из полюсов больше этого значения, то считаем, что полюс не сократился.
pole_coeff_max = 10**(-7)

e = symbols('e')

# Считываем панцеровские ответы
# Файл с ответами должен лежать в папке diags_N_loops
panzer_dict = dict()
with open(os.path.join(os.getcwd(), f'diags_{loops}_loops', f'diags_{loops}_loops_tv_answers.txt')) as f:
    data = f.readlines()
for i in range(len(data)):
    if i%2 == 0:
        panzer_dict[data[i].replace('\n','')] = ast.literal_eval(data[i+1].replace('\n',''))

print('!!!!!!!!!!!!!!!!!!!!!!!!!')
for i in panzer_dict:
    for j in panzer_dict[i]:
        panzer_dict[i][j] = sympify(panzer_dict[i][j])

#print(panzer_dict)


# Считываем ответы sector decomposition
# Файл с ответами должен лежать в папке sd_results
import re
sd_dict = dict()
with open(os.path.join(os.getcwd(), 'sd_results', f'{loops}loops.txt')) as f:
    data = f.readlines()
data = list(map(lambda x: x.replace('\n','').strip(), data))

nickel = None
tv_num = None
tv_ans = None
for line in data:
    if 'xxxxxxxxx' in line:
        nickel = line.split('/')[-1].split(' ')[0]
        sd_dict[nickel] = dict()

    if line[:2] == 'sd':
        tv_num_new = line.split('polys_')[1].split('_')[0]
        term_ans = line.split('txt')[1].strip()

        # Записываем абс погрешности коэффов в отдельный словарь
        error_dict = dict()  # {степень е: абс погр при соотв коэфф, ...}
        for i in range(len(term_ans)):
            if term_ans[i] == '(':
                l_pos = i  # left bracket position
            if term_ans[i] == ')':
                r_pos = i  # right bracket position
                error = sympify(term_ans[l_pos + 1:r_pos])  # считываем значение погр в скобках
                # Иногда после погр в скобках идет степень десятки: 1.1(1)e-05
                # Такое отлавливаем отдельно
                if r_pos+1!=len(term_ans) and term_ans[r_pos + 1] == 'e':
                    ten_power = term_ans[r_pos + 2:].split(' ')[0]
                    abs_error = error * 10 ** (-len(term_ans[:l_pos].split('.')[-1]) + int(ten_power))
                else:
                    abs_error = error * 10 ** (-len(term_ans[:l_pos].split('.')[-1]))  # абс знач погр с учетом коэффа

                # Вообще полином вроде всегда пишется в порядке увеличения степеней,
                # но на всякий вручную определим степень e при коэффициенте
                if len(term_ans[r_pos + 1:].split(' ')) == 1 or term_ans[r_pos + 1:].split(' ')[1] == '+':  # если после погр идет + (нет е), то степень первая e**0
                    e_power = 0
                else:
                    e_power = sympify(term_ans[r_pos + 1:].split('+')[0].split('*')[-1])
                error_dict[e_power] = abs_error
        error_dict.pop(-1, None)  # погрешность коэффа при полюсе не нужна


        term_ans = sympify(re.sub("\(.*?\)", "", term_ans))   # выкинули погрешности - они уже записаны отдельно
        if tv_num_new != tv_num or sd_dict[nickel] == dict():
            sd_dict[nickel][tv_num_new] = [term_ans, error_dict]
            tv_num = copy.deepcopy(tv_num_new)
        else:
            sd_dict[nickel][tv_num_new][0] += term_ans


# Проверяем малость коэффициентов при полюсах и отбрасываем их
for i in sd_dict:
    for j in sd_dict[i]:
        tv_expr = poly(sd_dict[i][j][0], 1/e)
        if tv_expr != 0:
            for pole_order in range(-1, -tv_expr.degree()-1, -1):
                if sd_dict[i][j][0].coeff(var('e'), pole_order) < pole_coeff_max:
                    sd_dict[i][j][0] -= sd_dict[i][j][0].coeff(var('e'), pole_order) * e ** (pole_order)
                else:
                    print(f'В диаграмме {i} в {j} полюс {-pole_order} подозрительно НЕ сокращается: {sd_dict[i][j][0]}')
                    sd_dict[i][j][0] -= sd_dict[i][j][0].coeff(var('e'), pole_order) * e ** (pole_order)  # все равно отбрасываем полюс, чтобы дальше проверить коэффы при полож степенях
                    # raise ValueError

#print(sd_dict)


# Сравниваем Панцера и sd
n = 5  # во сколько раз допустимо превышение погрешности sd при анализе разницы sd и панцера
print(panzer_dict)
for i in panzer_dict:
    for j in panzer_dict[i]:
        residual = panzer_dict[i][j] - sd_dict[i][j][0]
        for c in range(poly(residual).degree() + 1):
            if abs(residual.coeff(var('e'), c)) > n*sd_dict[i][j][1][c]:
                print(f'В диаграмме {i} в {j} разность коэффициентов при {c} степени методами sd и Панцера превышает погрешность sd: {residual}')
                print(f'Панцер: {panzer_dict[i][j]}')
                print(f'sd: {sd_dict[i][j]}')
                print('-----------------------------------------')



'''
#СТАРЫЙ ВАРИАНТ
# Сравниваем Панцера и sd (проверяется малость коэффов относ заданного порогового значения residual_coeff_max)
# Смотрим разность коэффициентов при всех неотриц степенях e по Панцера и sd.
# Если разность при любом из них больше этого значения, то считаем, что нашли отличие.
residual_coeff_max = 10**(-7)
for i in panzer_dict:
    for j in panzer_dict[i]:
        try:
            residual = poly(panzer_dict[i][j] - sd_dict[i][j], var('e'))
        except:
            print(i)
            print(j)
            print(panzer_dict[i][j] - sd_dict[i][j])
            residual = poly(panzer_dict[i][j] - sd_dict[i][j], var('e'))
        for c in residual.all_coeffs():
            if c > residual_coeff_max:
                print(f'В диаграмме {i} в {j} большая разность коэффициентов sd и Панцера: {residual}')
                print(f'Панцер: {panzer_dict[i][j]}')
                print(f'sd: {sd_dict[i][j]}')
                print('-----------------------------------------')
'''
