from sympy import *
import os
try:
    from config import *
except Exception as e:
    print(e)

if no_half_spine_account==True:
    dir_name = f'diags_{loops}_loops_no_halfspine'
else:
    dir_name = f'diags_{loops}_loops'

zeta2 = symbols('zeta2')
zeta3 = symbols('zeta3')
zeta4 = symbols('zeta4')

diag_res = []
sum = 0
diag_list = []
for address, dirs, files in os.walk(os.path.join(os.getcwd(), dir_name, 'ans')):
    for name in files:
        if name == f'order{order}_ans.txt':
            with open(os.path.join(address, name)) as f:
                diag_res.append(f.readlines()[0].replace('\n',''))

for d in diag_res:
    sum += sympify(d.replace(' ','').replace('\n','').replace('zeta[2]','zeta2').replace('zeta[3]','zeta3').replace('zeta[4]','zeta4'))
print(simplify(sum))

