# Собирает фейнмановские представления со всех n-петлевых диаграмм,
# разбивает их на полиномы первой степени для sector decomposition и
# пишет их в один файл
import os
from sympy import *

try:
    from config import *
except Exception as e:
    print(e)


def my_feyn_to_sd(feyn):
    sd_polys = []
    add_p = 1
    check_ext_mom = False
    for f in sympify(feyn).args:
        if f.func == (sympify('x**y')).func:
            if f.args[0].func == (sympify('x+y')).func:
                sd_polys.append([f.args[0], f.args[1]])
            else:
                number_aux = 1
                for ff in f.args[0].args:
                    if ff.is_number:
                        number_aux *= ff
                    elif ff.func == (sympify('x')).func:
                        sd_polys.append([number_aux*ff, f.args[1]])
                        number_aux = 1
                    elif ff.func == (sympify('x+y')).func:
                        sd_polys.append([number_aux*ff, f.args[1]])
                        number_aux = 1
                    elif ff==var('p')**2:
                        check_ext_mom = True
                        add_p *= (var('p')**2)**(f.args[1])
                        continue
                    else:
                        raise 'Что-то не так'

                if number_aux != 1:
                    sd_polys[-1][0] = sd_polys[-1][0] * number_aux

        #Проверки для отладки, если в нижнем raise вылезет ошибка
        '''
        else:
            raise 'Что-то не так'
        print('-------------------------')
        # Проверка на возможные ошибки:
        if check_ext_mom==False:
            raise 'Все-таки неправильно выделил вн импульс'
        if number_aux!=1:
            raise Exception(f'Неучет числового множителя {number_aux}')
        '''
    final_res = 1
    for factor in sd_polys:
        final_res *= (factor[0])**factor[1]
    if var('p') in final_res.free_symbols:
        raise Exception('В полиномах осталось p, хотя не должно было')

    final_res *= add_p
    if expand_power_base(feyn, force=True)!=expand_power_base(final_res, force=True):
        raise Exception('Не сошлось')

    #return sd_polys  # ПЕРВЫЙ ВАРИАНТ: p**2 засовывается в один из множителей sd_polys с такой же степенью
    return sd_polys, add_p


def my_feyn_to_sd_after_diff_p(feyn):
    sd_polys = []
    factor = 1  # сюда будут добавляться числовые множители и E, возникающие после дифф по p
    feyn = sympify(feyn)
    for f in feyn.args:
        if f.func == (sympify('x**y')).func:
            if f.args[0].func == (sympify('x+y')).func:
                sd_polys.append([f.args[0], f.args[1]])
            else:
                number_aux = 1
                f_arg = f.args[0].args
                if len(f_arg)==0:
                    sd_polys.append([f.args[0], f.args[1]])
                else:
                    for ff in f_arg:
                        if ff.is_number:
                            number_aux *= ff
                        elif ff.func == (sympify('x')).func:
                            sd_polys.append([number_aux*ff, f.args[1]])
                            number_aux = 1
                        elif ff.func == (sympify('x+y')).func:
                            sd_polys.append([number_aux*ff, f.args[1]])
                            number_aux = 1
                        else:
                            raise 'Что-то не так'

                if number_aux != 1:
                    sd_polys[-1][0] = sd_polys[-1][0] * number_aux

        elif f.func == var('x').func:
            sd_polys.append([f, 1])

        elif f.func == sympify(2).func:
            factor *= f

        elif str(f) == 'E':
            factor *= f

        elif f.atoms(Pow) == set():
            sd_polys.append([f, 1])

        else:
            print(f)
            raise 'Что-то не так'


    final_res = 1
    for poly in sd_polys:
        final_res *= (poly[0])**poly[1]
    final_res *= factor

    if expand_power_base(feyn, force=True)!=expand_power_base(final_res, force=True):
        raise Exception('Не сошлось')

    return factor, sd_polys


# my_feyn_to_sd - старая функция, где p**2 явно выделяется в предынтегральный множитель - работает только для основных вкладов (sd_polys_main_only = True)
# my_feyn_to_sd_new - новая функция, где p сразу кладется равным 1 - проще и должнаправильно работать для всех вкладов


if no_half_spine_account==True:
    dir_name = f'diags_{loops}_loops_no_halfspine'
else:
    dir_name = f'diags_{loops}_loops'

init_path = os.getcwd()

for address, dirs, files in os.walk(os.path.join(os.getcwd(), dir_name, 'ans')):
    for name in files:
        if name == 'info.txt':
            if sd_polys_main_only == True: # создает полиномы только для главного члена без дифф по p
                diag_output = []
                tv_num = 0
                with open(os.path.join(address, name)) as f:
                    lines = [line.replace('\n','') for line in f.readlines()]
                    #diag_output.append(lines[0])
                    #nickel = lines[0].replace(':','!').replace('|','-').replace('A','b')
                    for i in range(len(lines)):
                        if lines[i]=='------------------------------':
                            diff_vars = '['+lines[i - 1][1:-1].replace("'",'').replace(' ','').split('[')[1]
                            line_prep = lines[i - 1][1:-1].replace("'",'').replace(' ','').split(',')
                            sd_poly, add_p_factor = my_feyn_to_sd(line_prep[1])  # старый вариант
                            diag_output.append(str(sd_poly))
                            diag_output.append(diff_vars)
                            diag_output.append(str(sympify(line_prep[0])*add_p_factor))  # старый вариант
                            with open(os.path.join(address, f'sd_polys_tv{tv_num}.txt'), 'w') as f:
                                f.write('\n'.join(diag_output))
                            tv_num += 1
                            diag_output = []

            else:   # создает полиномы для всех членов (главного и всех вычитательных) после дифф по p
                diag_output = []
                tv_num = 0
                with open(os.path.join(address, name)) as f:
                    lines = [line.replace('\n','') for line in f.readlines()]
                    for i in range(len(lines)):
                        if lines[i]=='------------------------------':
                            tv_num += 1
                        if 'term' in lines[i]:
                            term_number = int(lines[i].replace('term','').replace(':',''))
                            diff_vars = '['+lines[i+2][1:-1].replace("'",'').replace(' ','').split('[')[1]
                            line_prep = lines[i+2][1:-1].replace("'",'').replace(' ','').split(',')
                            poly_prep = diff(sympify(line_prep[1]),var('p')).subs(var('p'), 1)
                            factor, sd_poly = my_feyn_to_sd_after_diff_p(poly_prep)
                            diag_output.append(str(sd_poly))
                            diag_output.append(diff_vars)
                            diag_output.append(str(-factor*sympify(line_prep[0])))  # минус от дифф по p
                            with open(os.path.join(address, f'sd_polys_tv{tv_num}_term{term_number}.txt'), 'w') as f:
                                f.write('\n'.join(diag_output))
                            diag_output = []
                            