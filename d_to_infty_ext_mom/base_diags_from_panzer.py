# Достаем ответы для n-loops диаграмм и сортируем по статическим топологиям.
# Для этого с помощью программы merge_answers_to_txt.py собираются ответы для всех нужных порядков по e
# Все соответствующие txt файлы с ответами должны быть подготовлены перед запуском этой программы
import os
import copy
from sympy import *
try:
    from config import *
except Exception as ex:
    print(ex)

# loops_res - порядок результата по количеству петель.
# Если loops_res = 3, то в двух петлях необходимо наличие файлов diags_2_loops_0order_ans.txt и diags_2_loops_1order_ans.txt,
#                     а в трех петлях diags_3_loops_0order_ans.txt
# Если loops_res = 4, то в двух петлях необходимо наличие файлов diags_2_loops_0order_ans.txt, diags_2_loops_1order_ans.txt и diags_2_loops_2order_ans.txt,
#                     в трех петлях diags_3_loops_0order_ans.txt и diags_3_loops_1order_ans.txt,
#                     а в четырех петлях diags_4_loops_0order_ans.txt
loops_res = 4  # вообще говоря интеруемся 4петлевым приближением
panzer_dict = dict()
zeta2 = symbols('zeta2')
zeta3 = symbols('zeta3')
zeta4 = symbols('zeta4')
for e_order in range(loops_res-loops+1):
    with open(os.path.join(os.getcwd(), f'diags_{loops}_loops', f'diags_{loops}_loops_{e_order}order_ans.txt')) as f:
        data = f.readlines()
    data = list(map(lambda x: x.replace('\n', '').strip(), data))
    for line in data:
        nickel = line.split(' ')[0]
        diag_ans = sympify(line.split(' ')[-1].replace('zeta[2]','zeta2').replace('zeta[3]','zeta3'))
        if nickel not in panzer_dict:
            panzer_dict[nickel] = diag_ans * var('e')**e_order
        else:
            panzer_dict[nickel] += diag_ans * var('e')**e_order

# Разбиваем по топологиям - суммируем вклады всех диаграмм с одинаковой статической топологией
panzer_topology_dict = dict()
for diag in panzer_dict:
    static_nickel = diag.split(':')[0]
    if static_nickel in panzer_topology_dict:
        panzer_topology_dict[static_nickel] += panzer_dict[diag]
    else:
        panzer_topology_dict[static_nickel] = copy.deepcopy(panzer_dict[diag])

for diag in panzer_topology_dict:
    panzer_topology_dict[diag] = collect(panzer_topology_dict[diag], var('e'))
    print(diag, '-->', panzer_topology_dict[diag])


# Проверка общей суммы диаграмм
#sum = sympify(0)
#for i in panzer_dict:
#    sum += panzer_dict[i]
#print(simplify(sum))
#print('1/32 + e*((-3)/32 - Pi^2/(48*2) + ln(2)/16) + e^2*(Pi^2/64 - Zeta(2)*ln(2)/8 + Zeta(3)/8 - (3*ln(2))/16 + ln(2)^2/16)')  # 2loop
#print('1/32 + Pi^2/(256*2) - e*(-9*Zeta(2)*ln(2)/(128*2) + (11*Zeta(2))/256 + Zeta(3)/16 - (3*ln(2))/32 + 41/512)')  # 3loop
