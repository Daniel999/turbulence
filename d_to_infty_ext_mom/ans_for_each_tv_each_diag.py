import os
from d_to_infty_class_ext_mom import D_to_infty_graph as D
from sympy import *
try:
    from config import *
except Exception as e:
    print(e)

if no_half_spine_account==True:
    dir_name = f'diags_{loops}_loops_no_halfspine'
else:
    dir_name = f'diags_{loops}_loops'

# Раскомментить для генерации файла с ответами в папке old_stretches_result
#dir_name = f'old_stretches_result\diags_{loops}_loops'

zeta2 = symbols('zeta2')
zeta3 = symbols('zeta3')
zeta4 = symbols('zeta4')
zeta5 = symbols('zeta5')
zeta1_3 = symbols('zeta[1,-3]')
start_path = os.getcwd()
ans_list = []

# Собирает в файл ответы для каждой вр версии каждой диаграммы (порядок и число петель берется из конфига)
# Ниже есть альтернативный кусок, который
for address, dirs, files in os.walk(os.path.join(start_path, dir_name, 'ans')):
    for dir in dirs:
        if 'x' in dir:
            diag_path = os.path.join(address,dir)
            ans_list.append(os.path.basename(diag_path)+'\n')
            diag_dict = dict()
            for file in os.listdir(diag_path):
                if 'tv' in file and 'ans' in file and f'order{order}' in file:
                    tv_num = file.split('tv')[1][0]
                    with open(os.path.join(diag_path, file)) as f:
                        str_expr = f.readlines()[0].replace('\n', '').replace('zeta[2]','zeta2').replace('zeta[3]','zeta3').replace('zeta[4]','zeta4').replace('zeta[5]','zeta5').replace('zeta[1,-3]','zeta1_3')
                        term_ans = sympify(str_expr)
                    if 'tv'+tv_num not in diag_dict:
                        diag_dict['tv'+tv_num] = term_ans
                    else:
                        diag_dict['tv' + tv_num] += term_ans

            # Следующие две строчки нужны для корректного считывания ответов через json в файле old_vs_new_stretches
            for tv_ans in diag_dict:
                diag_dict[tv_ans] = str(diag_dict[tv_ans])
            ans_list.append(str(diag_dict)+'\n')
            
#with open(os.path.join(start_path, dir_name, dir_name+f'_{order}order_tv_answers.txt'), 'a') as f:
with open(os.path.join(start_path, dir_name, f'diags_{loops}_loops_{order}order_tv_answers.txt'), 'a') as f:
    for a in ans_list:
        f.write(a)


# Cобирает в один файл ответы всех временных версий для всех посчитанных порядков
'''
e = var('e')
Pi = var('Pi')
for address, dirs, files in os.walk(os.path.join(start_path, dir_name, 'ans')):
    for dir in dirs:
        if 'x' in dir:
            diag_path = os.path.join(address,dir)
            ans_list.append(os.path.basename(diag_path)+'\n')
            diag_dict = dict()
            for file in os.listdir(diag_path):
                if 'tv' in file and 'ans' in file:
                    tv_num = file.split('tv')[1].split('_')[0]
                    with open(os.path.join(diag_path, file)) as f:
                        str_expr = f.readlines()[0].replace('\n', '').replace('zeta[2]','zeta2').replace('zeta[3]','zeta3').replace('zeta[4]','zeta4').replace('zeta[5]','zeta5')
                        term_ans = sympify(str_expr)

                    # Определяем степень e по названию файла
                    e_power = int(file.split('order')[1].split('_tv')[0])

                    if 'tv'+tv_num not in diag_dict:
                        diag_dict['tv'+tv_num] = term_ans * e**e_power
                    else:
                        diag_dict['tv' + tv_num] += term_ans * e**e_power

            for tv in diag_dict:
                diag_dict[tv] = diag_dict[tv].subs([(zeta2, 1.644934066848226), (zeta3, 1.202056903159594), (zeta4, 1.082323233711138), (Pi, 3.141592653589793238)])
                print(diag_dict[tv])
                diag_dict[tv] = diag_dict[tv].evalf()
                print(diag_dict[tv])
                diag_dict[tv] = str(diag_dict[tv])

            ans_list.append(str(diag_dict)+'\n')


with open(os.path.join(start_path, dir_name, dir_name+f'_tv_answers.txt'), 'a') as f:
    for a in ans_list:
        f.write(a)
'''





