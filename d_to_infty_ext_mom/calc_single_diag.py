import copy

from d_to_infty_class_ext_mom import D_to_infty_graph as D
from sympy import *
from copy import deepcopy
from sign_account import sign_account
from collections import defaultdict
import os, sys
import time
from functools import reduce

#e12|e3|34|5|55||:0A_dd_aA|0a_Aa|dd_aA|Aa|aA_dd|| # 3 loop 1 tv основная
#e12|e3|34|5|56|7|77||:0A_dd_aA|0a_Aa|dd_aA|Aa|dd_aA|Aa|aA_dd|| # 4 loop 1 tv
#e12|e3|34|5|56|7|78|9|99||:0A_dd_aA|0a_Aa|dd_aA|Aa|dd_aA|Aa|dd_aA|Aa|aA_dd||  # 4 loop 1 tv
#e12|e3|33||:0A_dd_aA|0a_Aa|aA_dd|| # 1 tv
#e12|34|35|4|5|e|:0A_aA_da|da_aA|dA_Ad|dd|aA|0a| # 2 bridges, one of them of 2 edges
#e12|e3|34|5|55||:0A_aA_dA|0a_da|dd_aA|Aa|aA_dd|| 2 subgraphs, no stretches
#e12|e3|34|5|55||:0A_aA_dA|0a_da|dd_aA|Aa|aA_dd|| - расписана у ЛЦ
#e12|23|4|45|6|e7|77||:0a_da_Aa|dd_Aa|Aa|dd_Ad|Aa|0A_aA|Aa_dd|| - 4loop, 21 tv
#e12|e3|33||:0A_dd_aA|0a_Aa|aA_dd|| - 2loop 1 tv (красивая)
#e12|e3|33||:0A_aA_da|0a_dA|Aa_dd|| - 2loop 1 tv

try:
    from config import *
except Exception as e:
    print(e)
from get_integrands_ext_mom_with_subgraph_filter import *
from sign_account import *
import graph_state as gs
import graphine, graph_state_config_with_fields
import networkx as nx

#diag = D('e12|23|3|e|:0A_aA_dd|aA_dd|aA|0a|')   # новая двух петлевая -1
#diag = D('e12|23|3|e|:0A_aA_da|dd_aA|Ad|0a|')   # двух петлевая -1
##diag = D('e12|23|3|e|:0A_aA_dA|dd_aA|ad|0a|')  # двух петлевая +1
#diag = D('e12|e3|34|5|55||:0A_dd_aA|0a_Aa|dd_aA|Aa|aA_dd||')


#diag = D('e12|e3|34|5|55||:0A_dd_aA|0a_Aa|dd_aA|Aa|aA_dd||'.replace('-','|').replace('x',':').replace('b','A'))
diag = D('e12|e3|34|5|56|7|78|9|99||:0A_dd_aA|0a_Aa|dd_aA|Aa|dd_aA|Aa|dd_aA|Aa|aA_dd||'.replace('-','|').replace('x',':').replace('b','A'))
print(diag.nickel.replace('|','-').replace(':','x').replace('A','b'))
print(diag.get_time_versions())
#diag.visualize_pdf()
R_sub_terms = substration(*integrand_with_ext_mom(diag, 0, no_half_spine_account))
feyn_terms = [feyn_repr(sub, diag) for sub in R_sub_terms]
i=1
for t in feyn_terms:
    print(i)
    print(t[0])
    print(t[1])
    print(t[2])
    print('------------------------------------------')
    i+=1




'''
for tv in diag.get_time_versions():
    if tv[0] == [3, 1, 0, 2, 5, 4]:
        print(tv)
        #print(integrand_with_ext_mom(diag, 0, no_half_spine_account))
        print(integrand_with_ext_mom_new(diag, 0, no_half_spine_account))
        #for term in substration(*integrand_with_ext_mom(diag, 5, no_half_spine_account)):
        #    print(term)

        #for term in substration(*integrand_with_ext_mom(diag, 5, no_half_spine_account)):
        #    print(term)
#print(diag.tv_over_main_and_subgraph_base_check(([0, 2, 3, 5, 4, 1], [e11|e|:0A_aA_dd|0a|, e12|e3|33||:0A_aA_da|0a_dA|Aa_dd||])))

#print(integrand_with_ext_mom(diag, 5, no_half_spine_account))
'''


