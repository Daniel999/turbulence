import time
import subprocess
try:
    from config import *
except Exception as e:
    print(e)

subprocess.run(["python", "prepare_integrands.py"])
time.sleep(5)
subprocess.run(["python", "maple_launch.py"])
