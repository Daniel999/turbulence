import sympy
import rggraphutil
e, u = sympy.var('e u')
c22, c21 = sympy.var('c22 c21')
c33, c32, c31 = sympy.var('c33 c32 c31')
c44, c43, c42, c41 = sympy.var('c44 c43 c42 c41')

## Znu := 1 - u/(8*e) + u^2*(c22/e^2 + c21/e) + u^3*(c33/e^3 + c32/e^2 + c31/e) + u^4*(c44/e^4 + c43/e^3 + c42/e^2 + c41/e)

Znu = sympy.numer(1) - u/(8*e) + u**2*(c22/e**2 + c21/e) + u**3*(c33/e**3 + c32/e**2 + c31/e) + u**4*(c44/e**4 + c43/e**3 + c42/e**2 + c41/e)

print(Znu)

Zg = (Znu**-3).series(u,0,5)
print(Zg)
## F := Znu*(1 + u*Zg*(1/(8*e) + ln(2)/8 + (Pi^2/48 + ln(2)^2/16)*e + (ln(2)*Pi^2/48 + ln(2)^3/48)*e^2) + u^2*Zg^2*((-0.015625)/(e^2) - 0.013848366/e - 0.10472755 - 0.06269719*e) + u^3*Zg^3*(0.003255209/e^3 + 0.00286285/e^2 + 0.0479697/e + 0.0145874) + u^4*Zg^4*(-0.00081380215/e^4 - 0.0006083894/e^3 - 0.021273590/e^2 + 0.00308764/e))
d22, d21, d20, d2m1 = sympy.var('d22 d21 d20 d2m1')
d33, d32, d31, d30 = sympy.var('d33 d32 d31 d30')
d44, d43, d42, d41 = sympy.var('d44 d43 d42 d41')

F = Znu*(sympy.numer(1) + u*Zg*(1/(8*e) + sympy.ln(2)/8 + (sympy.pi**2/48 + sympy.ln(2)**2/16)*e + (sympy.ln(2)*sympy.pi**2/48 + sympy.ln(2)**3/48)*e**2) + u**2*Zg**2*(d22/(e**2)+d21/e + d20 + d2m1*e) + u**3*Zg**3*(d33/e**3 + d32/e**2 + d31/e + d30) + u**4*Zg**4*(d44/e**4 +d43/e**3 + d42/e**2 + d41/e))

print(F)
G = F.series(u,0,2)